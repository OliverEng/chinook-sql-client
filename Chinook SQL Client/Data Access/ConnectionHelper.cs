﻿using Microsoft.Data.SqlClient;

namespace Chinook_SQL_Client.Data_Access
{
    /// <summary>
    /// A class for connecting to the Chinook database.
    /// </summary>
    public class ConnectionHelper
    {
        /// <summary>
        /// The connection string to the Chinook database.
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = @".", // <--- INSERT YOUR SQL SERVER NAME/INSTANCE HERE.
                InitialCatalog = "Chinook",
                IntegratedSecurity = true,
                TrustServerCertificate= true
            };
            return connectionStringBuilder.ConnectionString;
        }
    }
}
