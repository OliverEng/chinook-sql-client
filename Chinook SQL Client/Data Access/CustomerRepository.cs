﻿using Chinook_SQL_Client.Data_Access.Interfaces;
using Chinook_SQL_Client.Models;
using Microsoft.Data.SqlClient;

namespace Chinook_SQL_Client.Data_Access
{
    /// <summary>
    /// A class for the CustomerRepository containing methods for retrieving customer data from the Chinook database.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        /// <inheritdoc/>
        public Customer GetById(int id)
        {
            var customer = new Customer();
            const string sql =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE CustomerId = @customerId";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@customerId", id);
                using var reader = command.ExecuteReader();
                // Read
                while (reader.Read())
                {
                    customer.CustomerId = reader["CustomerId"] as int? ?? default;
                    customer.FirstName = reader["FirstName"].ToString();
                    customer.LastName = reader["LastName"].ToString();
                    customer.Country = reader["Country"].ToString();
                    customer.PostalCode = reader["PostalCode"].ToString();
                    customer.Phone = reader["Phone"].ToString();
                    customer.Email = reader["Email"].ToString();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customer;
        }

        /// <inheritdoc/>
        public List<Customer> GetByName(string name)
        {
            var customerList = new List<Customer>();
            const string sql =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE FirstName + ' ' + LastName LIKE '%'+@customerName+'%'";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@customerName", name);
                using var reader = command.ExecuteReader();
                // Read
                while (reader.Read())
                {
                    var customer = new Customer()
                    {
                        CustomerId = reader["CustomerId"] as int? ?? default,
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Country = reader["Country"].ToString(),
                        PostalCode = reader["PostalCode"].ToString(),
                        Phone = reader["Phone"].ToString(),
                        Email = reader["Email"].ToString()
                    };
                    customerList.Add(customer);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerList;
        }

        /// <inheritdoc/>
        public List<Customer> GetAll()
        {
            var customerList = new List<Customer>();
            const string sql =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                using var reader = command.ExecuteReader();
                // Read
                while (reader.Read())
                {
                    var customer = new Customer()
                    {
                        CustomerId = reader["CustomerId"] as int? ?? default,
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Country = reader["Country"].ToString(),
                        PostalCode = reader["PostalCode"].ToString(),
                        Phone = reader["Phone"].ToString(),
                        Email = reader["Email"].ToString()
                    };
                    customerList.Add(customer);
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerList;
        }

        /// <inheritdoc/>
        public List<Customer> GetPage(int limit, int offset)
        {
            var customerList = new List<Customer>();
            const string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                               "FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@limit", limit);
                command.Parameters.AddWithValue("@offset", offset);
                using var reader = command.ExecuteReader();
                // Read
                while (reader.Read())
                {
                    var customer = new Customer()
                    {
                        CustomerId = reader["CustomerId"] as int? ?? default,
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Country = reader["Country"].ToString(),
                        PostalCode = reader["PostalCode"].ToString(),
                        Phone = reader["Phone"].ToString(),
                        Email = reader["Email"].ToString()
                    };
                    customerList.Add(customer);
                }

            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerList;
        }

        /// <inheritdoc/>
        public bool Add(Customer customer)
        {
            var success = false;
            const string sql =
                "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);
                success = command.ExecuteNonQuery() > 0;

            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return success;
        }

        /// <inheritdoc/>
        public bool Update(Customer customer)
        {
            var success = false;
            const string sql =
                "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @CustomerId";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);
                success = command.ExecuteNonQuery() > 0;

            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return success;
        }

        /// <inheritdoc/>
        public List<CustomerCountry> GetCustomerCountries()
        {
            var customerCountryList = new List<CustomerCountry>();
            const string sql =
                "SELECT Country, COUNT(*) AS CustomerCount FROM Customer GROUP BY Country ORDER BY CustomerCount DESC";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                using var reader = command.ExecuteReader();

                // Read
                while (reader.Read())
                {
                    var customerCountry = new CustomerCountry()
                    {
                        Country = reader["Country"].ToString(),
                        CustomerCount = reader["CustomerCount"] as int? ?? default
                    };
                    customerCountryList.Add(customerCountry);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerCountryList;
        }


        /// <inheritdoc/>
        public CustomerGenre GetCustomerGenre(int id)
        {
            var customerGenre = new CustomerGenre();
            const string sql =
                "SELECT TOP 1 WITH TIES g.[Name] as GenreName, COUNT(g.[Name]) as GenreCount" +
                " FROM[Chinook].[dbo].[Invoice] i" +
                " JOIN[InvoiceLine] il ON i.InvoiceId = il.InvoiceId" +
                " JOIN[Track] t ON il.TrackId = t.TrackId" +
                " JOIN[Genre] g ON g.GenreId = t.GenreId" +
                " WHERE CustomerId = @CustomerId GROUP BY g.[Name] ORDER BY GenreCount DESC";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@CustomerId", id);
                using var reader = command.ExecuteReader();

                customerGenre.CustomerId = id;
                customerGenre.Genre = new List<string>();
                // Read
                while (reader.Read())
                {
                    customerGenre.Genre!.Add(reader["GenreName"].ToString()!);
                    customerGenre.GenreCount = reader["GenreCount"] as int? ?? default;
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerGenre;
        }


        /// <inheritdoc/>
        public List<CustomerSpender> GetCustomerSpenders()
        {
            var customerSpenderList = new List<CustomerSpender>();
            const string sql = "select CustomerId,sum(Total) as TotalSpent from[Invoice] group by CustomerId order by TotalSpent desc";

            try
            {
                // Connect
                using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
                connection.Open();
                // Execute
                using var command = new SqlCommand(sql, connection);
                using var reader = command.ExecuteReader();
                // Read
                while (reader.Read())
                {
                    var customerSpender = new CustomerSpender()
                    {
                        CustomerId = reader["CustomerId"] as int? ?? default,
                        TotalSpent = reader["TotalSpent"] as decimal? ?? default
                    };
                    customerSpenderList.Add(customerSpender);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
                throw;
            }

            return customerSpenderList;
        }
        
        // Delete method removed since it is not part of the assignment and it cannot delete with foreign-key relationships
        
        //public bool Delete(string id)
        //{
        //    var success = false;
        //    const string sql =
        //        "DELETE FROM Customer WHERE CustomerId = @CustomerId";

        //    try
        //    {
        //        // Connect
        //        using var connection = new SqlConnection(ConnectionHelper.GetConnectionString());
        //        connection.Open();
        //        // Execute
        //        using var command = new SqlCommand(sql, connection);
        //        command.Parameters.AddWithValue("@CustomerId", id);
        //        success = command.ExecuteNonQuery() > 0;

        //    }
        //    catch (SqlException e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }

        //    return success;
        //}
    }
}
