﻿using Chinook_SQL_Client.Models;

namespace Chinook_SQL_Client.Data_Access.Interfaces
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Get all customers from the database with their: id, first name, last name, country, postal code, phone number and email.
        /// </summary>
        /// <returns>A List of all Customer objects</returns>
        public List<Customer> GetAll();
        
        /// <summary>
        /// Get a customer's id, first name, last name, country, postal code, phone number, and email by id
        /// </summary>
        /// <param name="id">Id of the customer to get</param>
        /// <returns>A Customer Object</returns>
        public Customer GetById(int id);

        /// <summary>
        /// Get customers' id, first name, last name, country, postal code, phone number, and email by a customer's name or partial name
        /// </summary>
        /// <param name="name">The name to search for</param>
        /// <returns>a List of matching Customer objects</returns>
        public List<Customer> GetByName(string customerName);
        
        /// <summary>
        /// Add a new customer to the database containing first name, last name, country, postal code, phone, and email 
        /// </summary>
        /// <param name="customer">A Customer object of the new customer</param>
        /// <returns>True if succeeded or False if failed</returns>
        public bool Add(Customer customer);
        
        /// <summary>
        /// Update a customer in the database. Pass a new Customer object to the database which will update customer already in the database by id
        /// </summary>
        /// <param name="customer">A customer object to update with</param>
        /// <returns>True if succeeded or False if failed</returns>
        public bool Update(Customer customer);

        /// <summary>
        /// Get a list of customers' information of a certain range, determined by offset and limit
        /// </summary>
        /// <param name="limit">The number of items to get</param>
        /// <param name="offset">The offset of items to get</param>
        /// <returns>Returns a list of Customer objects</returns>
        public List<Customer> GetPage(int limit, int offset);

        
        /// <summary>
        /// Get a list of customers in each country ordered descending (high to low)
        /// </summary>
        /// <returns>a list of CustomerCountry objects sorted descending</returns>
        public List<CustomerCountry> GetCustomerCountries();

        /// <summary>
        /// Get a customers most popular genre by customer id, meaning the genre that corresponds to the most tracks from invoices associated to the customer
        /// </summary>
        /// <param name="id">The id of the customer</param>
        /// <returns>CustomerGenre object with the top genre/s and count/s</returns>
        public CustomerGenre GetCustomerGenre(int id);

        /// <summary>
        /// Get of list of customers who are the highest spenders, determined by total in invoice table, ordered descending (high to low)
        /// </summary>
        /// <returns>a list of CustomerSpender objects sorted descending</returns>
        public List<CustomerSpender> GetCustomerSpenders();

        // Delete method removed since it is not part of the assignment and it cannot delete with foreign-key relationships
        //public bool Delete(string id);

    }
}
