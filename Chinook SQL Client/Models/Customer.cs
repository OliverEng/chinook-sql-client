﻿namespace Chinook_SQL_Client.Models
{
    /// <summary>
    /// A class for the Customer model containing first name, last name, country, postal code, phone, and email
    /// </summary>
    public class Customer
    {
        public int? CustomerId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Country { get; set; }
        public string? PostalCode { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
    }
}
