﻿namespace Chinook_SQL_Client.Models
{
    /// <summary>
    /// A class for the CustomerCountry model containing a country and count of customers in the country.
    /// </summary>
    public class CustomerCountry
    {
        public string? Country { get; set; }
        public int? CustomerCount { get; set; }
    }
}
