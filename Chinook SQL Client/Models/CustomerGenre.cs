﻿namespace Chinook_SQL_Client.Models
{
    /// <summary>
    /// A class for the CustomerGenre model containing customer id, genre count and list of favorite genres.
    /// </summary>
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public int GenreCount { get; set; }
        public List<string>? Genre { get; set; }
 
    }
}
