﻿namespace Chinook_SQL_Client.Models
{
    /// <summary>
    /// A class for the CustomerSpender model containing customer id and total amount spent.
    /// </summary>
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public decimal? TotalSpent { get; set; }
    }
}
