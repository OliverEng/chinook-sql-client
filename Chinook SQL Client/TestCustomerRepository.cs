﻿using Chinook_SQL_Client.Data_Access;
using Chinook_SQL_Client.Data_Access.Interfaces;
using Chinook_SQL_Client.Models;

namespace Chinook_SQL_Client
{
    /// <summary>
    /// A Class of tests and print methods for the CustomerRepository class.
    /// </summary>
    public class TestCustomerRepository
    {
        private readonly ICustomerRepository _repository = new CustomerRepository();

        public void TestGetAll()
        {
            PrintCustomers(_repository.GetAll());
        }

        public void TestGetById(int id)
        {
            PrintCustomer(_repository.GetById(id));
        }

        public void TestGetByName(string name)
        {
            PrintCustomers(_repository.GetByName(name));
        }

        public void TestInsert()
        {
            var customer = new Customer()
            {
                FirstName = "John",
                LastName = "Doe",
                Country = "Denmark",
                PostalCode = "1000",
                Phone = "12345678",
                Email = "email@email.email"
            };
            Console.WriteLine(_repository.Add(customer) ? "Customer added" : "Customer not added");
            PrintCustomers(_repository.GetByName("John Doe"));
        }

        public void TestUpdate(int id)
        {
            var customer = _repository.GetById(id);

            // Example of just updating the name..
            customer.FirstName = "John";
            customer.LastName = "Johnson";

            Console.WriteLine(_repository.Update(customer) ? "Customer updated" : "Customer not updated");
            PrintCustomer(_repository.GetById(id));
        }

        public void TestGetCustomerPage(int limit, int offset)
        {
            PrintCustomers(_repository.GetPage(limit, offset));
        }

        public void TestGetCustomerCountry()
        {
            PrintCustomerCountries(_repository.GetCustomerCountries());
        }

        public void TestGetCustomerSpender()
        {
            var customerSpenderList = _repository.GetCustomerSpenders();

            foreach (var customerSpender in customerSpenderList)
            {
                PrintCustomerSpender(customerSpender);
            }
        }

        public void TestGetCustomerGenre(int id)
        {
            var customerGenre = _repository.GetCustomerGenre(id);
            PrintCustomersGenres(customerGenre);
        }

        private static void PrintCustomersGenres(CustomerGenre customerGenre)
        {
            Console.Write($"Most popular genre/s for id {customerGenre.CustomerId} are: ");
            foreach (var genre in customerGenre.Genre!)
            {
                Console.Write($"{genre}");
                if (customerGenre.Genre.IndexOf(genre) + 1 != customerGenre.Genre.Count())
                {
                    Console.Write(" and ");
                }
            }
            Console.Write($" with {customerGenre.GenreCount} purchases.");
        }

        private static void PrintCustomers(List<Customer> customers)
        {
            foreach (var customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        private static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"Id: {customer.CustomerId}, Name: {customer.FirstName} {customer.LastName} from {customer.Country} " +
                              $"with postal code {customer.PostalCode} and phone number {customer.Phone} and email {customer.Email}");
        }

        private static void PrintCustomerCountries(List<CustomerCountry> customerCountries)
        {
            foreach (var customerCountry in customerCountries)
            {
                PrintCustomerCountry(customerCountry);
            }
        }

        private static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"{customerCountry.Country} has {customerCountry.CustomerCount} users.");
        }

        private static void PrintCustomerSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"{customerSpender.CustomerId} has spent {customerSpender.TotalSpent} USD.");
        }
    }
}
