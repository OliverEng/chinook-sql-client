# CHINOOK-SQL-CLIENT
A simple SQL client with CRUD functionality, as well as sql files to create and populate a sample superhero database to showcase competences regarding Data Persistence and Access.

## Unused SQL-Database
In the root folder, .sql files to create and populate a sample superhero database can be found in the folder **SuperHeroesDb**.
The .sql files should be run in order 0-09. The database will not be used for the rest of the assignment, instead we will use a database called Chinook.

*An extra .sql file called 10_extraSelectView is there as well to view some of the generated data.*

![super_hero_database_image](SuperHeroesDb/SuperHeroesDbDiagram.jpg)

## Actual SQL-Database
The SQL Client works with a database called Chinook. To create the database, use **Chinook_SqlServer_AutoIncrementPKs.sql** found in the root folder.

## Usage
Before being able to use any functionality, the data source must be defined. In **\Chinook SQL Client\Data Access\ConnectionHelper.cs** set the DataSource property to be the name or address of the instance of SQL server.
>i.e. DESKTOP-TOOPDI6\SQLEXPRESS

*Test methods are part of the TestCustomerRepository class and can be run in program.cs where some method calls have been prepared.*

## Contributors

-   [Oliver Engermann (@OliverEng)](@OliverEng)
-   [Morten Bay Nielsen (@morten.bay)](@morten.bay)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

Noroff Accelerate, 2022.
