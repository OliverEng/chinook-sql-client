/*******************************************************************************
   Create Tables
********************************************************************************/
use SuperheroesDb;

CREATE TABLE Superhero 
(
    SuperheroId int CONSTRAINT [PK_Superhero] PRIMARY KEY IDENTITY(1,1),
    [Name] varchar(255) NULL UNIQUE,
    Alias varchar(255) NULL,
    Origin varchar(255) NULL,

); 

CREATE TABLE Assistant 
(
    AssistantId int CONSTRAINT [PK_Assistant] PRIMARY KEY IDENTITY(1,1),
    Name varchar(255) NULL UNIQUE,
); 

CREATE TABLE [Power] 
(
    PowerId int CONSTRAINT [PK_Power] PRIMARY KEY IDENTITY(1,1),
    Name varchar(255) NULL UNIQUE,
	Description varchar(255) NULL,
); 


