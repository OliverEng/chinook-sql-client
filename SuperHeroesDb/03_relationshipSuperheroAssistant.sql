/*******************************************************************************
   Create Table relationships
********************************************************************************/
use SuperheroesDb;

ALTER TABLE Assistant
ADD SuperheroId int CONSTRAINT FK_Assistant_Superhero FOREIGN KEY REFERENCES Superhero(SuperheroId) 
