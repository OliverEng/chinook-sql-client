/*******************************************************************************
   Create linking table
********************************************************************************/
use SuperheroesDb;

CREATE TABLE Superhero_PowerLink 
(

	SuperheroId int NOT NULL,
	PowerId int NOT NULL,
	CONSTRAINT PK_PowerLink PRIMARY KEY (SuperheroId , PowerId),
	CONSTRAINT FK_PowerLink_Superhero FOREIGN KEY (SuperheroId) REFERENCES Superhero(SuperheroId),
	CONSTRAINT FK_PowerLink_Power FOREIGN KEY (PowerId) REFERENCES [Power](PowerId)

); 
