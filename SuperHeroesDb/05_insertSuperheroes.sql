/*******************************************************************************
   Insert Superheros
********************************************************************************/
use SuperheroesDb;

INSERT INTO Superhero ([Name], Alias, Origin) VALUES ('Clark Kent', 'Superman','Krypton');
INSERT INTO Superhero ([Name], Alias, Origin) VALUES ('Bruce Wayne', 'Batman','Gotham');
INSERT INTO Superhero ([Name], Alias, Origin) VALUES ('Diana Prince', 'Wonder Woman','Themyscira');
