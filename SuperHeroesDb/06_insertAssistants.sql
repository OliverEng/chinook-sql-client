/*******************************************************************************
   Insert assistants
********************************************************************************/
use SuperheroesDb;

INSERT INTO Assistant ([Name], SuperheroId) VALUES ('Jimmy Olsen', 1);
INSERT INTO Assistant ([Name], SuperheroId) VALUES ('Alfred Pennyworth', 2);
INSERT INTO Assistant ([Name], SuperheroId) VALUES ('Steve Trevor', 3);


