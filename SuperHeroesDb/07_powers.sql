/*******************************************************************************
   Insert powers
********************************************************************************/
use SuperheroesDb;

INSERT INTO [Power] ([Name], [Description]) VALUES ('Flight', 'The power of true flight');
INSERT INTO [Power] ([Name], [Description]) VALUES ('Super Durability', 'Highly resistant to injury');
INSERT INTO [Power] ([Name], [Description]) VALUES ('Rich', 'Loaded with money');
INSERT INTO [Power] ([Name], [Description]) VALUES ('Martial Arts', 'Expert in the fighting Arts');

-- Superman has Flight, Super Durability and knows Martial Arts.
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (1,1);
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (1,2);
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (1,4);

-- Wonder Woman has Super Durability and knows Martial Arts.
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (2,2);
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (2,4);

-- Batman is rich and knows Martial Arts.
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (3,3);
INSERT INTO Superhero_PowerLink (SuperheroId, PowerId) VALUES (3,4);

