/*******************************************************************************
   Update Superhero
********************************************************************************/
use SuperheroesDb;

-- Changing Superman to The Incredible Superman
UPDATE Superhero SET [Name] = 'The Incredible Superman' WHERE SuperheroId = 1;
