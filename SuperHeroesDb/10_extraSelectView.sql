/*******************************************************************************
   Select Superheros
********************************************************************************/

SELECT 
  s.[Name] AS 'Name',
  s.[Alias] as 'Alias',
  a.[Name] as 'Assistants',
  STRING_AGG(p.[Name], ', ') AS 'Powers'
FROM Superhero s
JOIN Superhero_PowerLink sp
  ON s.SuperheroId = sp.SuperheroId
LEFT JOIN Assistant a
  ON s.SuperheroId = a.SuperheroId
JOIN [Power] p 
  ON p.PowerId = sp.PowerId AND s.SuperheroId = sp.SuperheroId
GROUP BY s.[Name], s.[Alias],a.[Name] ;

/*******************************************************************************
   Select Powers
********************************************************************************/

SELECT 
  p.[Name] AS 'Power Name',
  p.[Description] as 'Description',
  STRING_AGG(s.[Alias], ', ') AS 'Heros'
FROM Superhero s
JOIN Superhero_PowerLink sp
  ON s.SuperheroId = sp.SuperheroId
JOIN [Power] p 
  ON p.PowerId = sp.PowerId AND s.SuperheroId = sp.SuperheroId
GROUP BY p.[Name], p.[Description] ;


